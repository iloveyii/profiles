<?php
error_reporting(E_ALL);
ini_set('display_errors','On');

// include of configuration file 
include_once 'conf.php';
 
//include our models
include_once 'models/Database.php';
include_once 'models/Profile.php';
include_once 'models/Viewer.php';

//include our API Controller
include_once 'controllers/ApiController.php';
 
//wrap the whole thing in a try-catch block to catch any wayward exceptions!
try {
    
    // get the endpoint
    $endpoint = $_REQUEST['request']; unset($_REQUEST['request']);
    // get remaining parameters the POST/GET request
    $params = $_REQUEST;
    
    //get the controller and format it correctly so the first
    //letter is always capitalized
    $arEndpoint = explode('/', rtrim($endpoint, '/'));
    // check if request is valid, ie valid conroller and action
    if(count($arEndpoint) < 2) {
        $arError['status'] = 'Invalid Request';
        $arError['msg'] = 'Two endpoints implemented';
        $arError['endpoint1'] = 'http://domain.com/api/v1/profile/read&viewer_id=3';
        $arError['endpoint2'] = 'http://domain.com/api/v1/profile/viewers&viewer_id=3';
        ApiController::showError($arError, 404);
    }
    $controller = ucfirst(strtolower($arEndpoint[0])). 'Controller';
     
    //get the action and format it correctly so all the
    //letters are not capitalized, and append 'Action'
    $action = strtolower($arEndpoint[1]).'Action';
    
    //check if the controller exists. if not, throw an exception
    if( file_exists("controllers/{$controller}.php") ) {
        include_once "controllers/{$controller}.php";
    } else {
        // throw new Exception('Controller is invalid.');
        ApiController::showError("Controller is invalid: {$arEndpoint[0]}/{$arEndpoint[1]}", 404);
    }
     
    //create a new instance of the controller, and pass
    //it the parameters from the request
    $controller = new $controller($params);
     
    //check if the action exists in the controller. if not, throw an exception.
    if( method_exists($controller, $action) === false ) {
        // throw new Exception('Action is invalid.');
        ApiController::showError("Action is invalid: {$arEndpoint[0]}/{$arEndpoint[1]}", 404);
    }
     
    //execute the action
    $controller->$action();
     
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $result = array();
    $result['success'] = false;
    $result['errormsg'] = $e->getMessage();
    ApiController::showError($result,500);
}
 

