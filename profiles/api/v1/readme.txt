
***************************************************************
How to run
***************************************************************
1. Copy directory profiles to the root of webserver.
2. Point the Virtual domain to sub directory api/v1.
3. Browse to :
    i. http://test.loc/api/v1/profile/read&profile_id=2&viewer_id=1
    To read (and log visit to) a profile.

    ii. http://test.loc/api/v1/profile/viewers&viewer_id=2
    To get the profile views of the current user (i.e 2)
4. The application creates sqlite database automatically inside directory api/v1/data.
   Therefore it should be writeable by webserver user (eg www-data)
5. If you want to use mysql the required schema is at api/v1/data.

***************************************************************
Directory structure
***************************************************************
The code is stored in api/v1 to support v2 etc development.
The code implements mvc pattern so the controllers lies in directory controllers and models lies in models.
The code also implements singleton pattern (and abstract and factory) for db connectivity inside class Database. 

***************************************************************
Database Schema
***************************************************************
The database contains two tables profile and viewer. The profile table stores users profile information and
has two fields id and name for simplicity. 

The viewer table has four columns namely id, profile_id, viewer_id, dated. The field id is an autoincrement and
serves as primary key, profile_id is the id of the viewing profile and references id in the profile table. 
Whereas viewer_id is the profile id of the viewer and also references id of profile table.


a. Do you delete any data from the database?
    i. If yes, When? Why?
    ii. If no, Why?
    YES, as views of profile (no of rows in viewer table) increases the table size increase tremendously.
    So we delete the older records than 10 days.

b. Do you have any periodic task type of batch jobs to maintain data?
    i. If yes, Why needed?
    ii. If no, Why not needed?
    YES, in order to keep table viewer size smaller we run a batch (cron job) file maintenance.php which
    deletes older records in table viewer.

c. What type of compromise (for example; tradeoff between storage on disk vs
latency) did you see? What was your decision? Why?

    If the number of profiles increases in the table profile and there are two many views of these profiles
    consequently the size of table viewer will grow and a result performance of fetching viewers will decrease.
    We need to delete older records in viewer table to decrease its size but it we also lose information of 
    older views by the users. So there is tradeoff between the amount of information we keep and the size of
    of table.