<?php

class ProfileController extends ApiController {
    
    private $params;
     
    public function __construct($params)
    {
        $this->params = $params;
    }
     
    public function createAction() {
        //create a new profile
    }
    
    /**
     * Returns the viewers of the current user/profile
     */
    public function viewersAction() {
        /*
         *  get the viewer_id 
         */
        if( !isset($this->params['viewer_id'])) {
            ApiController::showError('Missing: viewer_id ! Correct format: http://domain.com/api/v1/profile/read&viewer_id=3',405);
        }
        $viewer_id=  $this->params['viewer_id'];
        
        /*
         *  Create model object, NB:this view means model View
         */
        $viewer = new Viewer();
        $data = $viewer->getViewers($viewer_id);
        
        /*
         *  send response to client  
         */
        $this->jsonResponse($data);
    }
    
    /**
     * Returns the profile of the given profile id
     * It also logs the viewer id in the viewer table
     */
    public function readAction() {
        
        /*
         *  get the viewer_id 
         */
        if( !isset($this->params['viewer_id'])) {
            ApiController::showError('Missing: viewer_id ! Correct format: http://domain.com/api/v1/profile/read&viewer_id=3&profile_id=2',405);
        }
        $viewer_id=  $this->params['viewer_id'];
        
        /*
         *  get the profile_id
         */
        if( !isset($this->params['profile_id'])) {
            ApiController::showError('Missing: profile_id ! Correct format: http://domain.com/api/v1/profile/read&viewer_id=3&profile_id=2',405);
        }
        $profile_id=  $this->params['profile_id'];
        
        /*
         *  Create model object, NB:this view means model View
         */
        $profile = new Profile();
        $data = $profile->read($profile_id, $viewer_id);
        
        /*
         *  send response to client  
         */
        $this->jsonResponse($data);
    }
    
    public function updateAction() {
        //update a profile
    }
     
    public function deleteAction() {
        //delete a profile
    }
}

