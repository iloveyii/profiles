<?php

abstract class ApiController {
    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';
    /**
     * Property: controller
     * The Controller requested in the URI. eg: /profile
     */
    protected $controller = '';
    /**
     * Property: action
     * The action in the controller eg a CRUD action like create, read, update, delete
     */
    protected $action = '';
    /**
     * Property: file
     * Stores the input of the PUT request
     */
    protected $file = Null;

    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */
    public function __construct() {
        
    }
    
    /**
     * Converts  the array data to json
     * 
     * @param array $data
     * @param string $status status eg success
     */
    public function jsonResponse($data, $status = 200) {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
        header("HTTP/1.1 " . $status . " " .  self::requestStatus($status));
        echo json_encode($data);
    }

    
    public function xmlResponse($data, $status = 200) { 
        // show data in xml format
    }
    
    /**
     * Displays error with relevant status code if eg the endpoint does not exist
     * 
     * @param mixed $data
     * @param integer $status http status codes
     */
    public static function showError($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . self::requestStatus($status));
        echo stripcslashes(json_encode($data));
        
        exit;
    }

    /*
     * Returns the string for the integer code
     */
    private static function requestStatus($code) {
        $status = array(  
            200 => 'OK',
            404 => 'Not Found',   
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        ); 
        
        return ($status[$code]) ? $status[$code] : $status[500]; 
    }


    
    
}