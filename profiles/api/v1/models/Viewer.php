<?php

/**
 * This is the model class for table "viewer".
 * This table contains data about who viewed what profile.
 *
 * The followings are the available columns in table 'viewer':
 * @property integer $id The auto increment primary key
 * @property integer $profile_id the id of the viewing profile
 * @property integer $viewer_id the id of the viewer
 * @property datetime $dated the date and time the viewer viewed a profile
 * 
 */
class Viewer extends Database { 
    
    /*
     *  Name of the database table
     */
    private $tableName='viewer';
    
    /**
     * Initialize the object
     */
    public function __construct() {
        
        parent::__construct();
        // set default time zone
        date_default_timezone_set('Europe/Stockholm');
    }

    /**
     * Returns the list of viewers for the specified $profile_id
     * 
     * @param integer $profile_id
     * 
     * @return array $response showing status and array of viewers for the profile id
     */
    public function getViewers ($viewer_id) {

        $response =[];
        $rowFound = FALSE;
        
        $sql = "SELECT * FROM {$this->tableName} WHERE profile_id = :profile_id LIMIT 10";
        $stmt = self::$dBh->prepare($sql); 

        if($stmt->execute([':profile_id'=>$viewer_id])) {
            $response['status']="success";
            $result =[];
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $result[]=$row;
                $rowFound = TRUE;
            }
            $response['data']= $rowFound ? $result : "Record for viewer_id = $viewer_id does not exist";
        } else {
            $response['status']="Oops something went wrong";
        }

        return $response;
    }

    /**
     * Delete all models/rows older than the specified number of days
     * 
     * @param integer $days
     * @return array $response array showing status and no of rows deleted
     */
    public function deleteAll ($days = 10) {

        $response =[];
        // Delete all older than this date
        $dated = date('Y-m-d H:i:s', strtotime("-{$days} days"));

        $sql = "DELETE  FROM {$this->tableName} WHERE dated < :dated";
        $stmt = self::$dBh->prepare($sql); 

        if($stmt->execute([':dated'=>$dated])) {
            $response['status']="success";
            $result['rows_deleted'] = $stmt->rowCount();
            $response['data']=$result;
        } else {
            $response['status']="Oops something went wrong";
        }

        return $response;
    }
}