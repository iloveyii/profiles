<?php

	 /**
	  * Interface for Database
	  * 
	  * The showTable displays a DB table in html table format
	  * 
	  * The setSQL is used to set sql query to any valid 
	  * user provided sql and then it will be displayed by showTable method
	  * 
	  * The setProbedSince sets the time in seconds for the devices  
	  * 
	  * @author Hazrat Ali <ali.sweden19@yahoo.com>
	  * Date: May 2010, VT10 BTH
	  */
	 interface IDatabase {
         public function connectDb ($profile_id, $user_id);
         public function profileviewers ($profile_id);
	 }
	 
	/**
	 * Database operations
	 * 
	 * Reguires database credentials to connect to a DB
	 * Displays/returns MySQL table/sql in html table format
	 * 
	 * @author Hazrat Ali <ali.sweden19@yahoo.com>
	 */
//	abstract class Database implements IDatabase {
	abstract class Database  {
		
        protected static $dBh;
		protected $debug;
		protected $sql;
		private $dbHost; private $dbName; private $dbUser; private $dbPass;
        
        /** set database type eg mysql, sqlite */
        private $dbType;


        /** 
	     * Performs initializations upon class instantiation 
	     * 
	     * @param string $dbHost Database host name 
	     * @param string $dbName Database name 
	     * @param string $dbUser Database user name 
	     * @param string $dbPass Database password
	     * @param string $debug set to true if you want to display messages, better for debug
	     * 
         * @return void 
	     */  
		public function __construct($dbType='sqlite', $debug=FALSE){
			
            /*
             *  get db credentials from conf.php
             */
            global $dbHost, $dbName, $dbUser, $dbPass;
            
            // set database types, mysql and sqlite implemented only
            $this->dbType = $dbType;
            
            // Set DB credentials
            $this->dbHost = $dbHost; 
            $this->dbName = $dbName; 
            $this->dbUser = $dbUser; 
            $this->dbPass = $dbPass;
            
			self::$dBh =$this->connectDB();
            
            // Set Debug flag
			$this->debug=$debug;
		}
		
		/**
         * Connects to Database 
         * 
         * @return database handle 
         */
		private function connectDB(){
            if(isset(self::$dBh)) {
                
               return self::$dBh; 
            }
            
            try {
                
                /**
                 * Connet to the required db
                 */
                switch ($this->dbType) {
                    case 'sqlite':
                        /*
                         * connect to SQLite database 
                         */
                        $dbPath = DATA_PATH . '/profile.sdb';
                        if(!file_exists($dbPath)) {
                           $this->populateDb($dbPath);
                        }

                        $dBh = new PDO("sqlite:{$dbPath}");
                        break;

                    case 'mysql':
                        $dBh = new PDO(
                            "mysql:host=".$this->dbHost.";dbname=".$this->dbName.";charset=utf8",$this->dbUser,$this->dbPass
                        );
                        break;
                    
                    default:
                        throw new Exception('No database type selected', 500);
                        break;
                }
                
                // Set errormode to exceptions
                $dBh->setAttribute(PDO::ATTR_ERRMODE,
                            PDO::ERRMODE_EXCEPTION);
                
                return $dBh; 
            }
            catch(PDOException $e)
                {
                echo $e->getMessage();
            }
            
			
		}
        
        private function populateDb($dbPath) {
            /*
             * open the database
             */
            $db = new PDO("sqlite:{$dbPath}");
            	 
            //create table1
            $db->exec("CREATE TABLE profile (
                        id INTEGER PRIMARY KEY, 
                        name TEXT
                        )
                    ");   
            //insert data
            $db->exec("INSERT INTO profile (id, name) VALUES (1, 'Ali');".
            "INSERT INTO profile (id, name) VALUES (2, 'Yam');" .
            "INSERT INTO profile (id, name) VALUES (3, 'Khan');");
            
            //create table2
            $db->exec("CREATE TABLE viewer (
                        id INTEGER PRIMARY KEY, 
                        profile_id INTEGER, 
                        viewer_id INTEGER, 
                        dated TEXT,
                        FOREIGN KEY(profile_id) REFERENCES profile(id)
                        FOREIGN KEY(viewer_id) REFERENCES profile(id)
                        )
                    "); 
            //insert data
            $db->exec("INSERT INTO viewer (id, profile_id, viewer_id, dated) VALUES (1,3,1,'2015-01-08 14:44:07');".
            "INSERT INTO viewer (id, profile_id, viewer_id, dated) VALUES (2,2,1,'2015-01-08 16:13:57');" .
            "INSERT INTO viewer (id, profile_id, viewer_id, dated) VALUES (3,2,3,'2015-01-08 16:14:09');");
        }
            
        /**
         * Checks if record exist in the given tableName
         * 
         * @param array $arField key value pair where key is column name and value is its value
         * @param string $tableName
         * 
         * @return boolean returns true if record found false otherwise
         */
        public function recordExist($arField , $tableName) {
            
            list($fieldName, $fieldValue) = each($arField);
            
            $sql = "SELECT * FROM {$tableName} WHERE {$fieldName} = {$fieldValue} LIMIT 1";
            $stmt = self::$dBh->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
            if($row) {
                return TRUE;
            }
            
            return FALSE;
        }
        
        public function __destruct() {
            
            /*
             * Destroy object
             */
            if(isset(self::$dBh)) {
                self::$dBh = NULL;
            }
            
        }
		
		
	}
