<?php

/**
 * This is the model class for table "profile".
 * This table contains the user profiles data.
 *
 * The followings are the available columns in table 'profile':
 * @property integer $id The autoincrement primary key
 * @property string $name
 * 
 */
class Profile extends Database { 
    
    /*
     * Name of the database table
     */
    private $tableName = 'profile';
    
    /**
     * Instantiate model
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Saves the log of viewer accessing a profile into viwers table
     * 
     * @param integer $profile_id profile id of the viewing profile
     * @param integer $viewer_id profile id of the viewer
     * 
     * @return array $response array showing status 
     */
    public function save ($profile_id, $viewer_id) {
            
        $response =[];
        $dated = date('Y-m-d H:i:s');

        $sql = "INSERT INTO viewer (profile_id, viewer_id, dated) VALUES (:profile_id, :viewer_id, :dated)";
        $stmt = self::$dBh->prepare($sql); 

        if($stmt->execute([':profile_id'=>$profile_id, ':viewer_id'=>$viewer_id, ':dated'=>$dated])) {
            $response['status']="success";
        } else {
            $response['status']="Oops something went wrong";
        }

        return $response;
    }
    
    /**
     * Returns the profile of the specified profile_id
     * It also logs the viwer to table view
     * 
     * @param integer $profile_id the profile id of the viewing profile
     * @param integer $viewer_id the profile if of the viewer
     * 
     * @return array $response showing status and data about profile
     */
    public function read($profile_id, $viewer_id) {
        
        $sql = "SELECT * FROM {$this->tableName} WHERE id = :profile_id LIMIT 1";
        $stmt = self::$dBh->prepare($sql); 

        if($stmt->execute([':profile_id'=>$profile_id])) {
            $response['status']="success";
            $result =[];
            $rowFound = FALSE;
            
            /**
             * Fetch the rows and put in an array
             */
            while ($rowProfile = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $result[]=$rowProfile;
                $rowFound = TRUE;
            }
            
            /**
             * Check if profile with profile_id exist
             */
            if($rowFound) {
                $response['data']= $result;
            } else {
                $response['data']= "Record for profile_id = $profile_id does not exist";
                return $response;
            }
            
            /**
             * Check if profile with viewer id exist
             */
            if($this->recordExist(['id'=>$viewer_id], $this->tableName)) {
               /*
                *  log the viewer
                */
               $this->save($profile_id, $viewer_id);
            } else {
                $response['data']= "Record for viewer_id = {$viewer_id} does not exist";
                return $response;
            }
            
            
        } else {
            $response['status']="Oops something went wrong";
        }

        return $response;
    }

}